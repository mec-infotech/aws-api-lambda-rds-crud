import React, { useRef, useState } from "react";
import {
  StatusBar,
  SafeAreaView,
  TextInput,
  Pressable,
  Modal,
  FlatList,
  View,
  TouchableWithoutFeedback,
  ScrollView,
  Keyboard,
  Dimensions
} from "react-native";
import styles from "./EventListStyle";
import { Header } from "../../components/basics/header";
import { CustomButton } from "../../components/basics/Button";
import { HiraginoKakuText } from "../../components/StyledText";
import { colors } from "../../styles/color";
import {
  Entypo,
  MaterialIcons,
  MaterialCommunityIcons,
  Feather,
} from "@expo/vector-icons";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { StatusChangeDialog } from "../status-change-dialog/StatusChangeDialog";
import { EventDeleteDialog } from "../event-delete-dialog/EventDeleteDialog";
import { Logout } from "../logout/Logout";
import { MultiDropBox } from "../../components/basics/MultiDropBox";
import { format } from "date-fns";
import { CustomCalendar } from "../../components/basics/Calendar";
import {
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

type Props = {
  navigation: NavigationProp<any, any>;
};
export const EventList = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId } = route.params as { userId: string };
  const [showInputs, setShowInputs] = useState(false);
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);
  const [isStatusChangeModalVisible, setIsStatusChangeModalVisible] = useState(false);
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [isLogOutModalVisible, setIsLogOutModalVisible] = useState(false);
  const [selectedOption, setSelectedOption] = useState("最終更新日が新しい");
  const [isFromStartDateCalendarVisible, setFromStartDateCalendarVisible] = useState(false);
  const [isToStartDateCalendarVisible, setToStartDateCalendarVisible] = useState(false);
  const [isFromEndDateCalendarVisible, setFromEndDateCalendarVisible] = useState(false);
  const [isToEndDateCalendarVisible, setToEndDateCalendarVisible] = useState(false);
  const [fromStartDate, setFromStartDate] = useState("");
  const [toStartDate, setToStartDate] = useState("");
  const [fromEndDate, setFromEndDate] = useState("");
  const [toEndDate, setToEndDate] = useState("");
  const fromStartDateInputRef = useRef(null);
  const toStartDateInputRef = useRef(null);
  const fromEndDateInputRef = useRef(null);
  const toEndDateInputRef = useRef(null);
  const fromStartDateRef = useRef(null);
  const toStartDateRef = useRef(null);
  const fromEndDateRef = useRef(null);
  const toEndDateRef = useRef(null);
  const { width } = Dimensions.get('window');
  const [x, setX] = useState(0);
  const [focusedInput, setFocusedInput] = useState("");

  const defaultOnPress = () => {
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };
  const handleCalendarPress = () => { };
  const toggleInputs = () => {
    setShowInputs(!showInputs);
  };

  //delete dialog
  const openEventDeleteModal = () => {
    setIsDeleteModalVisible(true);
  };
  const handleDeleteCancelButton = () => {
    setIsDeleteModalVisible(false);
    navigation.navigate("EventList", { userId: userId, eventId: 0 });
  };
  const handleEventDetail = () => {
    if (showDropdown || isDropdownVisible) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
      setIsDropdownVisible(false);
    } else {
      setIsDropdownVisible(false);
      setShowDropdown(false);
      setShowSecondDropdown(false);
      navigation.navigate("EventDetail", {
        userId: userId,
        eventId: 1,
        pgType: "EventList",
      });
    }
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };

  const handleTextInputPress = () => {
    if (showDropdown || isDropdownVisible) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
      setIsDropdownVisible(false);
    }
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };

  //logout Dialog
  const handleLogOut = () => {
    setIsDropdownVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setIsLogOutModalVisible(true);
  };
  const handleLogoutCancelButton = () => {
    setIsLogOutModalVisible(false);
    navigation.navigate("EventList", { userId: userId });
  };
  const handleLogoutButton = () => {
    setIsLogOutModalVisible(false);
    navigation.navigate("Login");
  };
  const handleCreateEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    navigation.navigate("EventCreate", { userId: userId });
  };
  const handleEditEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    navigation.navigate("EventEdit", { userId: userId, eventId: 1 });
  };
  const handleReproductionEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    navigation.navigate("EventCreate", { userId: userId });
  };
  const handleDeleteEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    openEventDeleteModal();
  };

  //status change dialog
  const [selectedStatusName, setSelectedStatusName] = useState("");
  const handleStatusChangeEvent = (status: string) => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setSelectedStatusName(status);
    setIsStatusChangeModalVisible(true);
    setIsDropdownVisible(false);
  };
  const handleCancelButton = () => {
    setIsStatusChangeModalVisible(false);
    navigation.navigate("EventList", { userId: userId, eventId: 0 });
  };

  const handleStatusChangePreparationEvent = (status: string) => {
    navigation.navigate("EventDetail", {
      userId: userId,
      eventId: 1,
      statusName: status,
    });
  };

  // Dropdown
  const dropdownData = [
    { label: "最終更新日が新しい", value: "newest" },
    { label: "最終更新日が古い", value: "oldest" },
  ];
  const handleSortingDropdownPress = () => {
    setIsDropdownVisible(!isDropdownVisible);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setDropBoxVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };

  const handleDropdownSelect = (value: any) => {
    setSelectedOption(value);
    setIsDropdownVisible(false);
  };
  // Edit Dropdown
  const [showDropdown, setShowDropdown] = useState(false);
  const [showSecondDropdown, setShowSecondDropdown] = useState(false);
  const [isDropBoxVisible, setDropBoxVisible] = useState(false);
  const handleDropDown = () => {
    setShowDropdown(!showDropdown);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    setDropBoxVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
  };

  // MultiDropBox
  const initialOptions = [
    { id: "1", label: "準備中" },
    { id: "2", label: "受付可能" },
    { id: "3", label: "受付終了" },
    { id: "4", label: "アーカイブ" },
  ];

  const initialSelectedId = ["1", "2", "3"];
  const dropDownRef = useRef(null);

  // Close dropdown when click outside
  const handleClosePopup = (event: any) => {
    if ((showDropdown && showSecondDropdown) || showDropdown) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
    }
    // Close sorting dropdown when click outside
    if (isDropdownVisible) {
      setIsDropdownVisible(false);
    }
    if (event.nativeEvent.target != dropDownRef) {
      if (isDropBoxVisible) {
        setDropBoxVisible(false);
      }
    }
    closeCalendar(event);
  };

  const handleFromStartDateCalendarPress = (event: any) => {
    (fromStartDateInputRef.current as any).focus();
    setFromStartDateCalendarVisible(!isFromStartDateCalendarVisible);
    setToStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    setFocusedInput("fromStartDateInput");
  };

  const handleToStartDateCalendarPress = (event: any) => {
    (toStartDateInputRef.current as any).focus();
    setToStartDateCalendarVisible(!isToStartDateCalendarVisible);
    setFromStartDateCalendarVisible(false);
    setFromEndDateCalendarVisible(false);
    setToEndDateCalendarVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
    (toStartDateInputRef.current as any).measure((x: number, y: number, width: number, height: number, pageX: number, pageY: number) => {
      setX(pageX);
    });
    setFocusedInput("toStartDateInput");
  };

  const handleFromEndDateCalendarPress = (event: any) => {
    (fromEndDateInputRef.current as any).focus();
    setFromEndDateCalendarVisible(!isFromEndDateCalendarVisible);
    setToEndDateCalendarVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
  };

  const handleToEndDateCalendarPress = (event: any) => {
    (toEndDateInputRef.current as any).focus();
    setToEndDateCalendarVisible(!isToEndDateCalendarVisible);
    setFromEndDateCalendarVisible(false);
    setFromStartDateCalendarVisible(false);
    setToStartDateCalendarVisible(false);
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDropdownVisible(false);
  };

  const handleFromStartDateSelect = (date: any) => {
    setFromStartDate(date);
    setFromStartDateCalendarVisible(false);
  };

  const handleToStartDateSelect = (date: any) => {
    setToStartDate(date);
    setToStartDateCalendarVisible(false);
  };

  const handleFromEndDateSelect = (date: any) => {
    setFromEndDate(date);
    setFromEndDateCalendarVisible(false);
  };

  const handleToEndDateSelect = (date: any) => {
    setToEndDate(date);
    setToEndDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (event.nativeEvent.target != fromStartDateInputRef.current && event.nativeEvent.target != fromStartDateRef) {
      if (isFromStartDateCalendarVisible) {
        setFromStartDateCalendarVisible(false);
      }
    }
    if (event.nativeEvent.target != toStartDateInputRef.current && event.nativeEvent.target != toStartDateRef) {
      if (isToStartDateCalendarVisible) {
        setToStartDateCalendarVisible(false);
      }
    }
    if (event.nativeEvent.target != fromEndDateInputRef.current && event.nativeEvent.target != fromEndDateRef) {
      if (isFromEndDateCalendarVisible) {
        setFromEndDateCalendarVisible(false);
      }
    }
    if (event.nativeEvent.target != toEndDateInputRef.current && event.nativeEvent.target != toEndDateRef) {
      if (isToEndDateCalendarVisible) {
        setToEndDateCalendarVisible(false);
      }
    }
  };

  // Pagination
  const DATA = [
    {
      id: 1,
      eventName: "出茂マラソン大会2024",
      venue: "会場1",
      eventPeriod: "",
      numberofPeople: "0",
    },
    {
      id: 2,
      eventName: "ピザ作り",
      venue: "会場1",
      eventPeriod: "2023/05/12 〜2023/05/12",
      numberofPeople: "0",
    },
    {
      id: 3,
      eventName: "福祉・保育のおしごと相談",
      venue: "センター",
      eventPeriod: "",
      numberofPeople: "100",
    },
    {
      id: 4,
      eventName: "出茂マラソン大会",
      venue: "A会場、B会場",
      eventPeriod: "",
      numberofPeople: "1",
    },
    {
      id: 5,
      eventName: "空き家対策セミナー",
      venue: "会場1",
      eventPeriod: "2023/05/12 〜2023/05/12",
      numberofPeople: "86",
    },
    {
      id: 6,
      eventName: "出茂マラソン大会2023",
      venue: "A会場",
      eventPeriod: "2023/05/12 〜2023/05/12",
      numberofPeople: "60",
    },
    {
      id: 7,
      eventName: "つくってみよう",
      venue: "福祉会館",
      eventPeriod: "",
      numberofPeople: "120",
    },
    {
      id: 8,
      eventName: "乳幼児健康相談",
      venue: "A会場、B会場、C会場、D会場",
      eventPeriod: "2023/05/12 〜2023/05/12",
      numberofPeople: "60",
    },
  ];

  const ITEMS_PER_PAGE = 8;
  const [page, setPage] = useState(1);

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.receptionStatusHeader}>
        <HiraginoKakuText style={styles.receptionStatusText}>
          受付状況
        </HiraginoKakuText>
        <MaterialCommunityIcons
          name="swap-vertical"
          size={20}
          color={colors.greyTextColor}
        />
      </View>
      <View style={[styles.eachHeadingContainer, styles.eventHeaderText]}>
        <HiraginoKakuText style={[styles.headerText]}>
          イベント名
        </HiraginoKakuText>
      </View>
      <View style={[styles.eachHeadingContainer, styles.venueHeaderText]}>
        <HiraginoKakuText style={[styles.headerText]}>会場</HiraginoKakuText>
      </View>
      <View style={[styles.eachHeadingContainer, styles.eventDateText]}>
        <HiraginoKakuText style={[styles.headerText]}>
          イベント期間
        </HiraginoKakuText>
      </View>
      <View style={styles.acceptedPersonHeader}>
        <HiraginoKakuText style={[styles.acceptedPersonHeaderText]}>
          受付した人
        </HiraginoKakuText>
        <MaterialCommunityIcons
          name="swap-vertical"
          size={20}
          color={colors.greyTextColor}
        />
      </View>
      <View></View>
    </View>
  );

  const renderTableItem = ({ item }: { item: any }) => (
    <Pressable style={styles.row} onPress={handleEventDetail}>
      <View style={[styles.cell, styles.receptionStatusBodyText]}>
        {item.id === 3 || item.id === 4 || item.id === 5 ? (
          <View style={styles.receptionStatusPrimaryLabel}>
            <HiraginoKakuText style={styles.receptionStatusPrimaryLabelText}>
              受付可能
            </HiraginoKakuText>
          </View>
        ) : item.id === 6 || item.id === 7 || item.id === 8 ? (
          <View style={styles.receptionStatusDefaultLabel}>
            <HiraginoKakuText style={styles.receptionStatusDefaultLabelText}>
              受付終了
            </HiraginoKakuText>
          </View>
        ) : (
          <View style={styles.receptionStatusLabel}>
            <HiraginoKakuText style={[styles.receptionStatusLabelText]}>
              準備中
            </HiraginoKakuText>
          </View>
        )}
      </View>
      <View style={[styles.cell, styles.eventTableBodyContainer]}>
        <HiraginoKakuText style={styles.eventTableBodyText} numberOfLines={1}>
          {item.eventName}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.venueBodyContainer]}>
        <HiraginoKakuText style={styles.venueBodyText} numberOfLines={1} normal>
          {item.venue}
        </HiraginoKakuText>
      </View>
      <View style={styles.eventDateBodyContainer}>
        <HiraginoKakuText style={styles.eventDateBodyText} normal>
          {item.eventPeriod}
        </HiraginoKakuText>
      </View>
      <View style={styles.acceptedPersonBodyContainer}>
        <HiraginoKakuText style={styles.acceptedPersonBodyText}>
          {item.numberofPeople}
        </HiraginoKakuText>
      </View>
      <Pressable style={styles.cell} onPress={handleDropDown}>
        <View style={styles.threeDotsChildContainer}>
          <MaterialIcons
            name="more-horiz"
            size={16}
            color={colors.greyTextColor}
          />
        </View>
      </Pressable>
    </Pressable>
  );

  const getPageData = () => {
    const startIndex = (page - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + ITEMS_PER_PAGE;
    return DATA.slice(startIndex, endIndex);
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        middleTitleName="LGaP受付 ポータルアプリ"
        buttonName="ログアウト"
        onPress={handleLogOut}
      ></Header>
      <ScrollView>
        <TouchableWithoutFeedback onPress={handleClosePopup}>
          <View style={styles.bodyContainer}>
            <View style={styles.bodyTitleContainer}>
              <HiraginoKakuText style={styles.bodyText}>
                イベント
              </HiraginoKakuText>
              <CustomButton
                text="イベント作成"
                onPress={handleCreateEvent}
                style={styles.bodyTitleButton}
                type="ButtonMPrimary"
              />
            </View>
            <View style={styles.mainInfoContainer}>
              <View style={[styles.firstChildContainer]}>
                <View style={styles.infoContainer}>
                  <View style={styles.parentInputContainer}>
                    <View style={styles.childInputContainer}>
                      <View style={styles.labelContainer}>
                        <HiraginoKakuText style={styles.labelText}>
                          イベント名
                        </HiraginoKakuText>
                      </View>
                      <View style={styles.inputContainer}>
                        <TextInput
                          style={styles.input}
                          placeholder="イベントタイトル"
                          placeholderTextColor={colors.placeholderTextColor}
                          onFocus={handleTextInputPress}
                        />
                      </View>
                    </View>
                    <View style={styles.childInputContainer}>
                      <View style={styles.labelContainer}>
                        <HiraginoKakuText style={styles.labelText}>
                          受付状況
                        </HiraginoKakuText>
                      </View>

                      <Pressable ref={dropDownRef}>
                        <MultiDropBox
                          initialOptions={initialOptions}
                          initialSelectedIds={initialSelectedId}
                          onCloseSortingDropdown={handleTextInputPress}
                        />
                      </Pressable>
                    </View>
                  </View>
                  <>
                    <Pressable
                      style={styles.dropdownContainer}
                      onPress={toggleInputs}
                    >
                      <HiraginoKakuText style={styles.dropdownText}>
                        詳細検索
                      </HiraginoKakuText>
                      <Entypo
                        name={showInputs ? "chevron-up" : "chevron-down"}
                        size={24}
                        color={colors.primary}
                        style={styles.iconStyle}
                      />
                    </Pressable>
                    {showInputs && (
                      <View style={styles.hiddenContainer}>
                        <View style={styles.parentInputContainer}>
                          <View style={styles.childInputContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                会場
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.inputContainer}>
                              <TextInput
                                style={styles.input}
                                placeholder="会場名"
                                placeholderTextColor={
                                  colors.placeholderTextColor
                                }
                                onFocus={handleTextInputPress}
                              />
                            </View>
                          </View>
                        </View>
                        <View style={[styles.mainDateContainer]}>
                          <View style={styles.parentDateContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                終了日
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.childDateContainer}>
                              <View style={styles.dateInputContainer}>
                                <TextInput
                                  ref={fromEndDateInputRef}
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={colors.placeholderTextColor}
                                  value={fromEndDate != "" ? format(new Date(fromEndDate), "yyyy/MM/dd") : fromEndDate}
                                  onPressIn={handleFromEndDateCalendarPress}
                                  onPointerDown={handleFromEndDateCalendarPress}
                                  showSoftInputOnFocus={false}
                                  onTouchStart={() => Keyboard.dismiss()}
                                  editable={false}
                                />
                                <Pressable
                                  ref={fromEndDateRef}
                                  style={styles.calendarIconContainer}
                                  onPress={handleFromEndDateCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                                {isFromEndDateCalendarVisible && (
                                  <CustomCalendar selectedDate={fromEndDate} onDateSelect={handleFromEndDateSelect} />
                                )}
                              </View>
                              <HiraginoKakuText style={styles.tildeText}>
                                〜
                              </HiraginoKakuText>
                              <View style={styles.secondDateInputContainer}>
                                <TextInput
                                  ref={toEndDateInputRef}
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={colors.placeholderTextColor}
                                  value={toEndDate != "" ? format(new Date(toEndDate), "yyyy/MM/dd") : toEndDate}
                                  onPressIn={handleToEndDateCalendarPress}
                                  onPointerDown={handleToEndDateCalendarPress}
                                  showSoftInputOnFocus={false}
                                  onTouchStart={() => Keyboard.dismiss()}
                                  editable={false}
                                />
                                <Pressable
                                  ref={toEndDateRef}
                                  style={styles.calendarIconContainer}
                                  onPress={handleToEndDateCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                                {isToEndDateCalendarVisible && (
                                  <CustomCalendar selectedDate={toEndDate} onDateSelect={handleToEndDateSelect} />
                                )}
                              </View>
                            </View>
                          </View>
                          <View style={styles.secondParentDateContainer}>
                            <View style={styles.labelContainer}>
                              <HiraginoKakuText style={styles.labelText}>
                                開始日
                              </HiraginoKakuText>
                            </View>
                            <View style={styles.childDateContainer}>
                              <View style={[styles.dateInputContainer,
                              (x + wp("30.25%") > width) && focusedInput === 'toStartDateInput' && { zIndex: -1 }]}>
                                <TextInput
                                  ref={fromStartDateInputRef}
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={colors.placeholderTextColor}
                                  value={fromStartDate != "" ? format(new Date(fromStartDate), "yyyy/MM/dd") : fromStartDate}
                                  onPressIn={handleFromStartDateCalendarPress}
                                  onPointerDown={handleFromStartDateCalendarPress}
                                  showSoftInputOnFocus={false}
                                  onTouchStart={() => Keyboard.dismiss()}
                                  editable={false}
                                />
                                <Pressable
                                  ref={fromStartDateRef}
                                  style={styles.calendarIconContainer}
                                  onPress={handleFromStartDateCalendarPress}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                                {isFromStartDateCalendarVisible && (
                                  <CustomCalendar selectedDate={fromStartDate} onDateSelect={handleFromStartDateSelect} />
                                )}
                              </View>
                              <HiraginoKakuText style={styles.tildeText}>
                                〜
                              </HiraginoKakuText>
                              <View style={[styles.secondDateInputContainer,
                              (x + wp("30.25%") > width) && { justifyContent: "flex-end" },
                              (x + wp("30.25%") > width) && focusedInput === 'fromStartDateInput' && { zIndex: -1 }]
                              }>
                                <TextInput
                                  ref={toStartDateInputRef}
                                  style={styles.dateInput}
                                  placeholder="日付を選択"
                                  placeholderTextColor={colors.placeholderTextColor}
                                  value={toStartDate != "" ? format(new Date(toStartDate), "yyyy/MM/dd") : toStartDate}
                                  onPressIn={handleToStartDateCalendarPress}
                                  onPointerDown={handleToStartDateCalendarPress}
                                  showSoftInputOnFocus={false}
                                  onTouchStart={() => Keyboard.dismiss()}
                                  editable={false}
                                />
                                <Pressable
                                  ref={toStartDateRef}
                                  onPress={handleToStartDateCalendarPress}
                                  style={styles.calendarIconContainer}
                                >
                                  <MaterialIcons
                                    name="calendar-today"
                                    size={24}
                                    color={colors.activeCarouselColor}
                                  />
                                </Pressable>
                                {isToStartDateCalendarVisible && (
                                  <CustomCalendar selectedDate={toStartDate} onDateSelect={handleToStartDateSelect} />
                                )}
                              </View>
                            </View>
                          </View>
                        </View>
                      </View>
                    )}
                  </>
                </View>
                <View style={styles.buttonContainer}>
                  <CustomButton
                    text="クリア"
                    onPress={defaultOnPress}
                    style={styles.grayMButton}
                    type="ButtonMediumGray"
                    textWidth={48}
                  />
                  <CustomButton
                    text="検索"
                    onPress={defaultOnPress}
                    style={styles.PrimaryMButton}
                    type="ButtonMPrimary"
                  />
                </View>
              </View>
              <View style={styles.parentPaginationContainer}>
                <View style={styles.topPaginationContainer}>
                  <View style={styles.countContainer}>
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      1-50 / 9,999 件中
                    </HiraginoKakuText>
                  </View>
                  <Pressable
                    style={styles.sortingContainer}
                    onPress={handleSortingDropdownPress}
                  >
                    <MaterialCommunityIcons
                      name="swap-vertical"
                      size={20}
                      color={colors.activeCarouselColor}
                    />
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      {selectedOption}
                    </HiraginoKakuText>
                    <Entypo
                      name="chevron-down"
                      size={20}
                      color={colors.greyTextColor}
                      style={styles.dropdownIconStyle}
                    />
                  </Pressable>

                  {/* Sorting Dropdown */}
                  {isDropdownVisible && (
                    <View style={styles.sortingDropdown}>
                      {dropdownData.map((item) => (
                        <Pressable
                          key={item.value}
                          style={styles.dropdownItem}
                          onPress={() => handleDropdownSelect(item.label)}
                        >
                          <HiraginoKakuText
                            style={styles.paginationCount}
                            normal
                          >
                            {item.label}
                          </HiraginoKakuText>
                        </Pressable>
                      ))}
                    </View>
                  )}
                </View>

                <View style={styles.tableContainer}>
                  {/* Render the header */}
                  {renderHeader()}

                  {/* Render the table items */}
                  {getPageData().map((item, index) => (
                    <View key={index}>{renderTableItem({ item })}</View>
                  ))}
                  {showDropdown && (
                    <View style={styles.dropdown}>
                      <Pressable
                        onPress={() =>
                          setShowSecondDropdown(!showSecondDropdown)
                        }
                      >
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          受付状況を変更
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable onPress={handleEditEvent}>
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          編集
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable onPress={handleReproductionEvent}>
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          複製
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable onPress={handleDeleteEvent}>
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.deleteBodyText]}
                        >
                          削除
                        </HiraginoKakuText>
                      </Pressable>
                    </View>
                  )}
                  {showSecondDropdown && (
                    <View style={styles.secondDropdown}>
                      <Pressable onPress={defaultOnPress}>
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          準備中
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable
                        onPress={() => handleStatusChangeEvent("受付可能")}
                      >
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          受付可能
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable
                        onPress={() => handleStatusChangeEvent("受付終了")}
                      >
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          受付終了
                        </HiraginoKakuText>
                      </Pressable>
                      <Pressable
                        onPress={() => handleStatusChangeEvent("アーカイブ")}
                      >
                        <HiraginoKakuText
                          normal
                          style={[styles.optionText, styles.bodyText]}
                        >
                          アーカイブ
                        </HiraginoKakuText>
                      </Pressable>
                    </View>
                  )}
                </View>
                <View style={styles.onChangePageContainer}>
                  <View style={styles.previousButtonsContainer}>
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.skipBackButton}
                      type="ButtonSDisable"
                      icon={
                        <Feather
                          name="skip-back"
                          size={20}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="center"
                    />
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.chevronLeftButton}
                      type="ButtonSDisable"
                      icon={
                        <Feather
                          name="chevron-left"
                          size={20}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="center"
                    />
                  </View>
                  <View style={styles.pageNumberContainer}>
                    <CustomButton
                      text="1"
                      onPress={defaultOnPress}
                      style={styles.numOneButton}
                      type="ButtonSPrimary"
                    />
                    <CustomButton
                      text="2"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                    <CustomButton
                      text="3"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                    <HiraginoKakuText style={styles.threeDots} normal>
                      …
                    </HiraginoKakuText>
                    <CustomButton
                      text="50"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                  </View>
                  <View style={styles.nextButtonsContainer}>
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.rightButtons}
                      type="ButtonSGray"
                      icon={
                        <Feather
                          name="chevron-right"
                          size={20}
                          color={colors.textColor}
                        />
                      }
                      iconPosition="center"
                    />
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.rightButtons}
                      type="ButtonSGray"
                      icon={
                        <Feather
                          name="skip-forward"
                          size={20}
                          color={colors.textColor}
                        />
                      }
                      iconPosition="center"
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>

      {isStatusChangeModalVisible && (
        <StatusChangeDialog
          statusName={selectedStatusName}
          onCancelButtonPress={handleCancelButton}
          onChangeButtonPress={handleCancelButton}
        />
      )}
      {isDeleteModalVisible && (
        <EventDeleteDialog
          onCancelButtonPress={handleDeleteCancelButton}
          onDeleteButtonPress={handleDeleteCancelButton}
        />
      )}
      {isLogOutModalVisible && (
        <Logout
          onCancelButtonPress={handleLogoutCancelButton}
          onLogoutButtonPress={handleLogoutButton}
        />
      )}
    </SafeAreaView>
  );
};
