import { invokeLambda } from "../../config/commonCRUDfuns";

// READ DATA
export const fetchData = async () => {
    try {

        // SELECT ALL
        // const { statusCode, body } = await invokeLambda('read', 'user_table', null, null, '');

        // SELECT With Conditions
        const conditions = {
            age: { value: 30, operator: '>' },
            name: 'John',
        };
        const { statusCode, body } = await invokeLambda('read', 'user_table', null, conditions, '');

        if (statusCode === 200) {
            const { result } = JSON.parse(body);
            return result;
        } else {
            console.error('Error fetching data:', body);
        }
    } catch (error) {
        console.error('Error fetching data:', error);
    }
};


// UPDATE Data
export const updateData = async () => {

    const conditions = {
        name: 'John',
    };

    const newData = {
        age: 100,
    };
    try {
        const { statusCode, body } = await invokeLambda('update', 'user_table', newData, conditions);

        if (statusCode === 200) {
            const { result } = JSON.parse(body);
            return result;
        } else {
            console.error('Error updating data:', body);
            return null;
        }
    } catch (error) {
        console.error('Error updating data:', error);
        return null;
    }
};