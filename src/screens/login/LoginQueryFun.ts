import { executeQuery } from "../../config/commonCRUD-query";

// READ Data
export const fetchData = async () => {
  const method = 'POST'; 
  const queryString = 'SELECT * FROM user_table WHERE age > 20 ORDER BY name';
  return executeQuery(method, queryString);
};

export const updateData = async () => {
  const method = 'PUT';
  const queryString = "UPDATE user_table SET age = 120 WHERE name = 'John'"; 
  return executeQuery(method, queryString);
}

export const insertData = async () => {
  const method="POST";
  const queryString = "INSERT INTO user_table (age, name) VALUES (27, 'Thandar');";
  return executeQuery(method, queryString);
}

export const deleteData = async () => {
  const method="POST";
  const queryString = "DELETE FROM user_table WHERE id IN (34, 36, 39, 40);";
  return executeQuery(method, queryString);
}