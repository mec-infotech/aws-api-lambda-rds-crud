import { invokeLambdaThroughAPI } from "../../config/commonCRUDfuns";
// READ DATA
export const fetchData = async () => {
    try {

        // SELECT ALL
        // const { statusCode, body } = await invokeLambdaThroughAPI('POST','read', 'user_table', null, null, '');   

        // SELECT With Conditions
        const conditions = {
            age: { value: 30, operator: '>' },
            name: 'John',
        };
        const { statusCode, body } = await invokeLambdaThroughAPI('POST','read', 'user_table', null, conditions, '');

        if (statusCode === 200) {
            const parsedBody = JSON.parse(body);
            const result = parsedBody.result;
            return result;
        } else {
            console.error('Error fetching data:', body);
        }
    } catch (error) {
        console.error('Error fetching data:', error);
    }
};

// UPDATE Data
