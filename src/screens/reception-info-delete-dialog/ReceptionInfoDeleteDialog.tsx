import React, { useState } from "react";
import { View } from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import ModalComponent from "../../components/basics/ModalComponent";
import styles from "./ReceptionInfoDeleteDialogStyles";
import { FontAwesome } from "@expo/vector-icons";

type DeleteProps = {
  onCancelButtonPress?: () => void;
  onDeleteButtonPress?: () => void;
  registerType?: string;
  roleName?: string;
};
export const ReceptionInfoDeleteDialog = (props: DeleteProps) => {
  const [isModalVisible, setModalVisible] = useState(true);

  var registrationType = props.registerType; //individual(本人) or group(同時に受付を行っている人)
  var role = props.roleName; // 受付代表者 or 一緒に受付

  var titleText = "";
  titleText =
    registrationType == "individual"
      ? "＜人の氏名＞を削除しますか？"
      : role == "受付代表者"
      ? "＜人の氏名＞を削除すると、同時に受付を行った人も削除されますがよろしいですか？"
      : "＜人の氏名＞を削除しますか？";
  return (
    <View>
      {isModalVisible && (
        <ModalComponent
          text={titleText}
          firstButtonText="キャンセル"
          secondButtonText="削除する"
          secondButtonType="ButtonMDanger"
          onFirstButtonPress={props.onCancelButtonPress}
          onSecondButtonPress={props.onDeleteButtonPress}
          toggleModal={props.onCancelButtonPress}
          secondButtonWidth={104}
          secondBtnTextWidth={64}
        >
          <View style={styles.bodyContainer}>
            {(registrationType == "individual" ||
              (registrationType == "group" && role == "一緒に受付")) && (
              <HiraginoKakuText style={styles.bodyText} normal>
                削除された人はもとに戻すことはできません。
              </HiraginoKakuText>
            )}
            {registrationType == "group" && role == "受付代表者" && (
              <HiraginoKakuText style={styles.bodyText} normal>
                「受付をした人」を削除すると、同時に受付を行った人も削除されます。削除された人はもとに戻すことはできません。
                {"\n"}
                {"\n"}
                以下の人が削除されます。{"\n"}
                <View style={styles.bulletTextContainer}>
                  <FontAwesome name="circle" style={styles.bullet} />
                  <HiraginoKakuText style={styles.bodyText} normal>
                    ＜人の氏名＞
                  </HiraginoKakuText>
                </View>{" "}
                {"\n"}
                <View style={styles.bulletTextContainer}>
                  <FontAwesome name="circle" style={styles.bullet} />
                  <HiraginoKakuText style={styles.bodyText} normal>
                    ＜人の氏名＞
                  </HiraginoKakuText>
                </View>{" "}
                {"\n"}
                <View style={styles.bulletTextContainer}>
                  <FontAwesome name="circle" style={styles.bullet} />
                  <HiraginoKakuText style={styles.bodyText} normal>
                    ＜人の氏名＞
                  </HiraginoKakuText>
                </View>{" "}
                {"\n"}
              </HiraginoKakuText>
            )}
          </View>
        </ModalComponent>
      )}
    </View>
  );
};
