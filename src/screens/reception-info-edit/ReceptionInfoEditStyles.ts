import { Platform, StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { colors } from "../../styles/color";
import { LabelLargeBold, LabelLargeRegular } from "../../styles/typography";
import { fonts } from "../../styles/font";

const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
  },
  container: {
    paddingHorizontal: hp("10.7"),
    paddingTop: hp("3.55"),
    paddingBottom: hp("10.7"),
    gap: hp("2.15"),
    backgroundColor: "#EEEFF1",
    ...Platform.select({
      web: {
        height: "90%",
      },
    }),
  },
  bodyContainer: {
    gap: hp("2.15"),
    flexDirection: "row",
  },
  leftContainer: {
    marginBottom: hp("18"),
    backgroundColor: colors.secondary,
    borderRadius: hp("0.7"),
    ...Platform.select({
      web: {
        width: 268,
      },
      ios: {
        flex: 1,
      },
      android: {
        flex: 1,
      },
    }),
  },
  leftTitleContainer: {
    paddingVertical: hp("1.05"),
    paddingHorizontal: hp("1.45"),
    borderBottomWidth: 1,
    borderColor: colors.borderColor,
    flexDirection: "row",
    gap: 4,
  },
  leftButtonsContainer: {
    padding: hp("1.45"),
    gap: hp("2.85"),
  },
  leftSubButtonContainer: {
    gap: hp("1.45"),
  },
  rightContainer: {
    flex: 2.47,
    gap: hp("2.15"),
  },
  rightBodyContainer: {
    backgroundColor: colors.secondary,
    borderRadius: hp("0.7"),
    paddingTop: hp("2.85"),
    paddingBottom: hp("5.7"),
    paddingHorizontal: hp("3.55"),
    gap: hp("2"),
    height: hp("77"),
    ...Platform.select({
      web: {
        gap: hp("2.85"),
        height: "auto",
      },
      android: {
        height: hp("72"),
      },
    }),
  },
  nameContainer: {
    gap: hp("0.7"),
  },
  rightLabelContainer: {
    gap: hp("0.65"),
    flexDirection: "row",
  },
  nameInputBoxContainer: {
    flexDirection: "row",
    gap: hp("2.15"),
  },
  datePickerContainer: {
    gap: hp("0.6"),
    justifyContent: "center",
    width: wp("21.95"),
    zIndex: 1,
  },
  calendarIconContainer: {
    right: 10,
    width: 20,
    height: 24,
    position: "absolute",
  },
  genderContainer: {
    gap: hp("0.65"),
    zIndex: 0,
  },
  radioBtnContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: hp("0.9"),
    gap: hp("0.7"),
    ...Platform.select({
      android: {
        paddingVertical: 0,
      },
    }),
  },
  postCodeContainer: {
    gap: hp("0.7"),
    zIndex: 0,
  },
  postCodeBoxContainer: {
    height: hp("3.95"),
    width: wp("28.72"),
    gap: hp("1.25"),
    flexDirection: "row",
  },
  addressContainer: {
    gap: hp("0.7"),
    zIndex: 0,
  },
  relationshipContainer: {
    gap: hp("0.7"),
    zIndex: 0,
  },
  dropdownContainer: {
    gap: hp("0.6"),
    justifyContent: "center",
    width: wp("21.95"),
  },
  dropdownIconContainer: {
    right: 10,
    width: 20,
    height: 24,
    position: "absolute",
  },
  saveBtnContainer: {
    flex: 1,
    gap: hp("0.8"),
    height: 44,
  },
  errorContainer: {
    display: "flex",
    flexDirection: "row",
  },
  radioNoAnsBox: {
    flex: 1.75,
  },
  genderRadioPanel: {
    width: wp("24.1"),
    height: hp("3.95"),
    flexDirection: "row",
    gap: hp("2.15"),
  },
  textInput: {
    width: "100%",
    height: hp("3.95"),
    borderWidth: 1,
    borderColor: colors.borderColor,
    borderRadius: hp("0.55"),
    paddingVertical: hp("0.5"),
    paddingHorizontal: hp("0.8"),
    gap: hp("0.61"),
    fontFamily: fonts.FontRegular.fontFamily,
    fontSize: LabelLargeRegular.size,
    fontWeight: "300",
    justifyContent: "center",
    ...Platform.select({
      web: {
        paddingVertical: hp("0.9"),
      },
    }),
  },
  nameInput: {
    flex: 1,
  },
  postCodeInput: {
    width: wp("19.45"),
  },
  leftButton: {
    width: "100%",
    height: hp("3.85"),
    borderWidth: 1,
    borderColor: colors.borderColor,
    borderRadius: hp("0.35"),
    paddingHorizontal: hp("1.45"),
    gap: hp("0.7"),
    fontFamily: fonts.FontRegular.fontFamily,
    fontSize: LabelLargeRegular.size,
    fontWeight: "300",
    justifyContent: "center",
  },
  saveBtn: {
    width: 104,
    backgroundColor: "#346DF4",
    position: "absolute",
    top: 0,
    right: 0,
    height: 44,
    borderRadius: hp("0.35"),
    paddingVertical: hp("0.9"),
    paddingHorizontal: hp("1.8"),
    gap: hp("0.61"),
    alignItems: "center",
    justifyContent: "center",
  },
  btnSelected: {
    borderColor: "#346DF4",
    borderWidth: 2,
    backgroundColor: "#F0F8FF",
  },
  calendarIcon: {
    width: 20,
    height: 22,
    position: "absolute",
    top: 0,
    right: 0,
  },
  radioButtonIcon: {
    width: 24,
    flexDirection: "row",
  },
  dropdownIcon: {
    width: 20,
    height: 22,
    position: "absolute",
    top: 0,
    right: 0,
  },
  radioButton: {
    width: 18,
    height: 18,
  },
  searchBtn: {
    width: wp("8.05"),
    borderWidth: 2,
    borderColor: colors.blue,
    borderRadius: hp("0.7"),
    paddingHorizontal: hp("1.1"),
    gap: hp("0.7"),
    justifyContent: "center",
    ...Platform.select({
      web: {
        paddingVertical: hp("0.9"),
        paddingHorizontal: hp("1.25"),
      },
    }),
  },
  bodyText: {
    color: colors.textColor,
    fontSize: LabelLargeRegular.size,
  },
  titleText: {
    fontWeight: "600",
    fontSize: 18,
  },
  subTitleText: {
    lineHeight: LabelLargeBold.lineHeight,
    fontWeight: "600",
    fontSize: LabelLargeBold.size,
    color: "#515867",
  },
  radioBtnText: {
    fontSize: 16,
  },
  LabelLargeBold: {
    fontSize: LabelLargeBold.size,
    fontWeight: "600",
    lineHeight: LabelLargeBold.lineHeight,
  },
  searchBtnText: {
    color: colors.blue,
    alignSelf: "center",
  },
  dropdownText: {
    fontSize: LabelLargeBold.size,
  },
  saveBtnText: {
    color: colors.secondary,
  },
  dropdownButtonText: {
    fontSize: 16,
  },
  optionText: {
    padding: 8,
    fontSize: 16,
    borderBottomWidth: 1,
    borderBottomColor: "lightgrey",
  },
  errorText: {
    color: colors.danger,
    fontSize: LabelLargeRegular.size,
    flex: 1,
  },
  dropdown: {
    position: "absolute",
    top: 30,
    right: 0,
    left: 0,
    backgroundColor: "white",
    borderRadius: hp("0.55"),
    borderWidth: 1,
    borderColor: "black",
    marginTop: 5,
  },
});

export default styles;
