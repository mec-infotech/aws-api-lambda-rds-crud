import React, { useState } from "react";
import styles from "./EventDetailStyle";
import { Header } from "../../components/basics/header";
import { colors } from "../../styles/color";
import { HiraginoKakuText } from "../../components/StyledText";
import { CustomButton } from "../../components/basics/Button";
import PieChart from "react-native-pie-chart";
import {
  StatusBar,
  SafeAreaView,
  View,
  ScrollView,
  Pressable,
  TouchableWithoutFeedback,
} from "react-native";
import {
  Entypo,
  MaterialIcons,
  MaterialCommunityIcons,
  AntDesign,
  Feather,
} from "@expo/vector-icons";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { StatusChangeDialog } from "../status-change-dialog/StatusChangeDialog";
import { EventDeleteDialog } from "../event-delete-dialog/EventDeleteDialog";

type Props = {
  navigation: NavigationProp<any, any>;
};

export const EventDetail = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId, eventId, pgType, editPgType } = route.params as {
    userId: string;
    eventId: number;
    pgType: string;
    editPgType: string;
  };
  const defaultOnPress = () => {};

  const handleBack = () => {
    if (pgType == "Create") {
      navigation.navigate("EventCreate", {
        userId: userId,
        eventId: 0,
      });
    } else if (pgType == "EventList") {
      navigation.navigate("EventList", {
        userId: userId,
        eventId: 0,
      });
    } else {
      navigation.navigate("EventEdit", {
        userId: userId,
        eventId: eventId,
        pgType: "EventDetail",
      });
    }
  };

  //StatusChange Dialog
  const [isStatusChangeModalVisible, setIsStatusChangeModalVisible] =
    useState(false);
  const [selectedStatusName, setSelectedStatusName] = useState("");
  const handleStatusChangeEvent = (status: string) => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setSelectedStatusName(status);
    setIsStatusChangeModalVisible(true);
  };
  const handleCancelButton = () => {
    setIsStatusChangeModalVisible(!setIsStatusChangeModalVisible);
    navigation.navigate("EventDetail", {
      userId: userId,
      eventId: eventId,
      pgType: pgType,
    });
  };
  const handleEventEdit = () => {
    navigation.navigate("EventEdit", {
      userId: userId,
      eventId: eventId,
      pgType: pgType,
      editPgType: "EventDetail",
    });
  };
  const handleReceptionInfoDetail = () => {
    if (showDropdown) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
    } else {
      setShowDropdown(false);
      setShowSecondDropdown(false);
      navigation.navigate("ReceptionInfoDetail", {
        userId: userId,
        eventId: eventId,
        receptionId: 1,
        pgType: pgType,
      });
    }
  };

  const handleReproductionEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    navigation.navigate("EventCreate", { userId: userId, pgType: pgType });
  };

  //delete dialog
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const handleDeleteEvent = () => {
    setShowDropdown(false);
    setShowSecondDropdown(false);
    setIsDeleteModalVisible(true);
  };
  const handleDeleteCancelButton = () => {
    setIsDeleteModalVisible(false);
    navigation.navigate("EventDetail", {
      userId: userId,
      eventId: 1,
      pgType: pgType,
    });
  };
  const handleDeleteButton = () => {
    setIsDeleteModalVisible(false);
    navigation.navigate("EventList", {
      userId: userId,
      eventId: 0,
    });
  };

  // PieChart
  const widthAndHeight = 84;
  const firstSeries = [140, 20, 0];
  const secondSeries = [25, 10, 0];
  const thirdSeries = [30, 20, 50];
  const sliceColor = [
    colors.skyBlueColor,
    colors.lightBlueColor,
    colors.paleBlueColor,
  ];

  // Edit Dropdown
  const [showDropdown, setShowDropdown] = useState(false);
  const [showSecondDropdown, setShowSecondDropdown] = useState(false);
  const handleDropDown = () => {
    setShowDropdown(!showDropdown);
    setShowSecondDropdown(false);
  };

  // Close dropdown when click outside
  const handleCloseDropdown = () => {
    if ((showDropdown && showSecondDropdown) || showDropdown) {
      setShowDropdown(false);
      setShowSecondDropdown(false);
    }
  };

  // Pagination
  const DATA = [
    {
      id: 1,
      receptionDateTime: "2023/05/12 14:00",
      receptionMethod: "アプリ",
      acceptedPerson: "出茂　進次郎",
      gender: "男性",
      dateOfBirth: "1985/11/02（999歳）",
      address: "出茂県出茂市公園通り1丁目12番3号メゾンde茂A-203号室",
    },
    {
      id: 2,
      receptionDateTime: "2023/05/12 14:00",
      receptionMethod: "アプリ",
      acceptedPerson: "出茂　ふみ",
      gender: "女性",
      dateOfBirth: "1986/02/21（99歳）",
      address: "出茂県出茂市公園通り1丁目12番3号メゾンde茂A-203号室",
    },
    {
      id: 3,
      receptionDateTime: "2023/05/12 14:00",
      receptionMethod: "手入力",
      acceptedPerson: "散布　瑠音",
      gender: "未入力",
      dateOfBirth: "2012/04/02（24歳）",
      address: "出茂県出茂市公園通り1丁目12番3号メゾンde茂A-203号室",
    },
    {
      id: 4,
      receptionDateTime: "2023/05/12 14:00",
      receptionMethod: "アプリ",
      acceptedPerson: "散布　るか",
      gender: "男性",
      dateOfBirth: "2013/06/20（0歳）",
      address: "出茂県出茂市公園通り1丁目12番3号メゾンde茂A-203号室",
    },
    {
      id: 5,
      receptionDateTime: "2023/05/12 14:00",
      receptionMethod: "アプリ",
      acceptedPerson: "出茂　進次郎",
      gender: "男性",
      dateOfBirth: "1985/11/02（999歳）",
      address: "出茂県出茂市公園通り1丁目12番3号メゾンde茂A-203号室",
    },
    {
      id: 6,
      receptionDateTime: "2023/05/12 14:00",
      receptionMethod: "アプリ",
      acceptedPerson: "出茂　ふみ",
      gender: "女性",
      dateOfBirth: "1986/02/21（99歳）",
      address: "出茂県出茂市公園通り1丁目12番3号メゾンde茂A-203号室",
    },
    {
      id: 7,
      receptionDateTime: "2023/05/12 14:00",
      receptionMethod: "手入力",
      acceptedPerson: "散布　瑠音",
      gender: "未入力",
      dateOfBirth: "2012/04/02（24歳）",
      address: "出茂県出茂市公園通り1丁目12番3号メゾンde茂A-203号室",
    },
    {
      id: 8,
      receptionDateTime: "2023/05/12 14:00",
      receptionMethod: "アプリ",
      acceptedPerson: "散布　るか",
      gender: "男性",
      dateOfBirth: "2013/06/20（0歳）",
      address: "出茂県出茂市公園通り1丁目12番3号メゾンde茂A-203号室",
    },
  ];

  const ITEMS_PER_PAGE = 8;
  const [page, setPage] = useState(1);

  const renderHeader = () => (
    <View style={styles.header}>
      <View style={styles.receptionDateTimeHeader}>
        <HiraginoKakuText style={styles.headerText}>受付日時</HiraginoKakuText>
        <MaterialIcons
          name="arrow-upward"
          size={24}
          color={colors.greyTextColor}
        />
      </View>
      <View style={styles.receptionMethodHeader}>
        <HiraginoKakuText style={styles.headerText}>受付方法</HiraginoKakuText>
        <MaterialCommunityIcons
          name="swap-vertical"
          size={20}
          color={colors.greyTextColor}
        />
      </View>
      <View style={styles.identificationHeader}>
        <HiraginoKakuText style={styles.headerText}>本人確認</HiraginoKakuText>
        <MaterialCommunityIcons
          name="swap-vertical"
          size={20}
          color={colors.greyTextColor}
        />
      </View>
      <View style={[styles.receptionTargetHeader]}>
        <HiraginoKakuText style={[styles.headerText]}>
          受付対象
        </HiraginoKakuText>
      </View>
      <View style={styles.acceptedPersonHeader}>
        <HiraginoKakuText style={[styles.headerText]}>
          受付した人
        </HiraginoKakuText>
      </View>
      <View style={styles.genderHeader}>
        <HiraginoKakuText style={[styles.headerText]}>性別</HiraginoKakuText>
      </View>
      <View style={styles.dateOfBirthHeader}>
        <HiraginoKakuText style={[styles.headerText]}>
          生年月日
        </HiraginoKakuText>
      </View>
      <View style={styles.addressHeader}>
        <HiraginoKakuText style={[styles.headerText]}>住所</HiraginoKakuText>
      </View>
    </View>
  );

  const renderTableItem = ({ item }: { item: any }) => (
    <Pressable style={styles.row} onPress={handleReceptionInfoDetail}>
      <View style={[styles.cell, styles.receptionDateTimeContainer]}>
        <HiraginoKakuText style={styles.receptionDateTimeText} normal>
          {item.receptionDateTime}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.receptionMethodContainer]}>
        <HiraginoKakuText style={styles.receptionMethodText} normal>
          {item.receptionMethod}
        </HiraginoKakuText>
      </View>

      <View style={[styles.cell, styles.identificationContainer]}>
        {item.id === 2 || item.id === 6 ? (
          <View style={styles.identification}>
            <AntDesign
              name="checkcircleo"
              size={24}
              color={colors.activeCarouselColor}
              style={styles.checkcircleStyle}
            />
          </View>
        ) : item.id === 3 || item.id === 7 ? (
          <View style={styles.identification}>
            <HiraginoKakuText style={styles.noneText}>ー</HiraginoKakuText>
          </View>
        ) : (
          <View style={styles.identification}>
            <AntDesign
              name="checkcircle"
              size={24}
              color={colors.greenColor}
              style={styles.checkcircleStyle}
            />
          </View>
        )}
      </View>

      <View style={[styles.cell, styles.receptionTargetContainer]}>
        {item.id === 1 || item.id === 4 || item.id === 5 || item.id === 8 ? (
          <View style={styles.representativeLabel}>
            <HiraginoKakuText style={styles.representativeLabelText}>
              受付代表
            </HiraginoKakuText>
          </View>
        ) : item.id === 2 || item.id === 6 ? (
          <View style={styles.receptionTogetherLabel}>
            <HiraginoKakuText style={styles.receptionTogetherText}>
              一緒に受付
            </HiraginoKakuText>
          </View>
        ) : (
          <View style={styles.onlyPersonLabel}>
            <HiraginoKakuText style={styles.onlyPersonLabelText}>
              本人のみ
            </HiraginoKakuText>
          </View>
        )}
      </View>
      <View style={[styles.cell, styles.acceptedPersonContainer]}>
        <HiraginoKakuText style={styles.acceptedPersonText} normal>
          {item.acceptedPerson}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.genderContainer]}>
        <HiraginoKakuText style={styles.genderText} normal>
          {item.gender}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.dateOfBirthContainer]}>
        <HiraginoKakuText style={styles.dateOfBirthText} normal>
          {item.dateOfBirth}
        </HiraginoKakuText>
      </View>
      <View style={[styles.cell, styles.addressContainer]}>
        <HiraginoKakuText style={styles.addressText} numberOfLines={1} normal>
          {item.address}
        </HiraginoKakuText>
      </View>
    </Pressable>
  );

  const getPageData = () => {
    const startIndex = (page - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + ITEMS_PER_PAGE;
    return DATA.slice(startIndex, endIndex);
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <StatusBar barStyle="dark-content" />
      <Header
        titleName="もどる"
        middleTitleName="出茂市マラソン 2023"
        buttonName=""
        hasButton={false}
        icon={<Entypo name="chevron-left" size={24} color={colors.primary} />}
        iconPosition="front"
        onPressLeft={handleBack}
      ></Header>
      <ScrollView>
        <TouchableWithoutFeedback onPress={handleCloseDropdown}>
          <View>
            <View style={styles.firstParentContainer}>
              <View style={styles.contentsContainer}>
                <View style={styles.btnsContainer}>
                  <View style={styles.eventNameContainer}>
                    <View style={styles.eventLabel}>
                      <HiraginoKakuText style={styles.receptionStatus}>
                        受付終了
                      </HiraginoKakuText>
                    </View>
                    <HiraginoKakuText style={styles.eventTitleText}>
                      出茂市マラソン 2023
                    </HiraginoKakuText>
                  </View>

                  <View style={styles.editBtnContainer}>
                    <CustomButton
                      text=""
                      onPress={handleEventEdit}
                      style={styles.pencilIcon}
                      type="ButtonMediumGray"
                      icon={
                        <MaterialCommunityIcons
                          name="pencil"
                          size={16}
                          color="black"
                        />
                      }
                      iconPosition="center"
                    />
                    <CustomButton
                      text=""
                      onPress={handleDropDown}
                      style={styles.moreIcon}
                      type="ButtonMediumGray"
                      icon={
                        <MaterialIcons
                          name="more-horiz"
                          size={16}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="center"
                    />

                    {showDropdown && (
                      <View style={styles.dropdown}>
                        <Pressable
                          onPress={() =>
                            setShowSecondDropdown(!showSecondDropdown)
                          }
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            受付状況を変更
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable onPress={handleReproductionEvent}>
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            複製
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable onPress={handleDeleteEvent}>
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.deleteBodyText]}
                          >
                            削除
                          </HiraginoKakuText>
                        </Pressable>
                      </View>
                    )}
                    {showSecondDropdown && (
                      <View style={styles.secondDropdown}>
                        <Pressable>
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            準備中
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          onPress={() => handleStatusChangeEvent("受付可能")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            受付可能
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          onPress={() => handleStatusChangeEvent("受付終了")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            受付終了
                          </HiraginoKakuText>
                        </Pressable>
                        <Pressable
                          onPress={() => handleStatusChangeEvent("アーカイブ")}
                        >
                          <HiraginoKakuText
                            normal
                            style={[styles.optionText, styles.bodyText]}
                          >
                            アーカイブ
                          </HiraginoKakuText>
                        </Pressable>
                      </View>
                    )}
                  </View>
                </View>
                <View style={styles.showDataContainer}>
                  <View style={styles.eventPeriodContainer}>
                    <HiraginoKakuText style={styles.eventPeriodLabel} normal>
                      イベント期間：
                    </HiraginoKakuText>
                    <HiraginoKakuText style={styles.eventPeriodData} normal>
                      2023/05/12 〜2023/05/12
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.venueContainer}>
                    <HiraginoKakuText style={styles.venueLabel} normal>
                      会場：
                    </HiraginoKakuText>
                    <HiraginoKakuText style={styles.venueData} normal>
                      会場A、会場B、会場C
                    </HiraginoKakuText>
                  </View>
                </View>
              </View>
              <View style={styles.tabsContainer}>
                <View style={[styles.venueTab, styles.currentTab]}>
                  <HiraginoKakuText
                    style={[styles.venueText, styles.currentText]}
                  >
                    全て
                  </HiraginoKakuText>
                </View>
                <View style={styles.venueTab}>
                  <HiraginoKakuText style={styles.venueText} normal>
                    会場A
                  </HiraginoKakuText>
                </View>
                <View style={styles.venueTab}>
                  <HiraginoKakuText style={styles.venueText} normal>
                    会場B
                  </HiraginoKakuText>
                </View>
                <View style={styles.venueTab}>
                  <HiraginoKakuText style={styles.venueText} normal>
                    会場C
                  </HiraginoKakuText>
                </View>
              </View>
            </View>
            <View style={styles.secondParentContainer}>
              <View style={styles.statisticsContainer}>
                <View style={styles.topContainer}>
                  <HiraginoKakuText style={styles.statisticsText}>
                    統計情報
                  </HiraginoKakuText>
                  <CustomButton
                    text="CSVダウンロード"
                    onPress={defaultOnPress}
                    style={styles.csvDownloadButton}
                    type="ButtonMediumGray"
                    textWidth={133}
                  />
                </View>
                <View style={styles.lineSeparator} />
                <View style={styles.numberOfPeople}>
                  <HiraginoKakuText style={styles.numOfPeopleLabel}>
                    合計人数
                  </HiraginoKakuText>
                  <View style={styles.numOfPeopleContainer}>
                    <HiraginoKakuText style={styles.numOfPeopleValue}>
                      80
                    </HiraginoKakuText>
                    <HiraginoKakuText style={styles.hitoLabel}>
                      人
                    </HiraginoKakuText>
                  </View>
                </View>
                <View style={styles.ellipseContainer}>
                  <View style={styles.pieChartContainer}>
                    <PieChart
                      widthAndHeight={widthAndHeight}
                      series={firstSeries}
                      sliceColor={sliceColor}
                      coverRadius={0.45}
                      coverFill={"#FFF"}
                    />
                    <View style={styles.userCountsContainer}>
                      <View style={styles.firstCountContainer}>
                        <View style={styles.firstCountRectangle} />
                        <HiraginoKakuText style={styles.firstCountText}>
                          アプリ：
                          <HiraginoKakuText style={{ color: colors.primary }}>
                            60
                          </HiraginoKakuText>
                          人
                        </HiraginoKakuText>
                      </View>
                      <View style={styles.secondCountContainer}>
                        <View style={styles.secondCountRectangle} />
                        <HiraginoKakuText style={styles.secondCountText}>
                          手入力：
                          <HiraginoKakuText style={{ color: colors.primary }}>
                            20
                          </HiraginoKakuText>
                          人
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <View style={styles.verticalSeparator} />
                  <View style={styles.pieChartContainer}>
                    <PieChart
                      widthAndHeight={widthAndHeight}
                      series={secondSeries}
                      sliceColor={sliceColor}
                      coverRadius={0.45}
                      coverFill={"#FFF"}
                    />
                    <View style={styles.userCountsContainer}>
                      <View style={styles.firstCountContainer}>
                        <View style={styles.firstCountRectangle} />
                        <HiraginoKakuText style={styles.firstCountText}>
                          成年：
                          <HiraginoKakuText style={{ color: colors.primary }}>
                            70
                          </HiraginoKakuText>
                          人
                        </HiraginoKakuText>
                      </View>
                      <View style={styles.secondCountContainer}>
                        <View style={styles.secondCountRectangle} />
                        <HiraginoKakuText style={styles.secondCountText}>
                          未成年：
                          <HiraginoKakuText style={{ color: colors.primary }}>
                            10
                          </HiraginoKakuText>
                          人
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <View style={styles.verticalSeparator} />
                  <View style={styles.pieChartContainer}>
                    <PieChart
                      widthAndHeight={widthAndHeight}
                      series={thirdSeries}
                      sliceColor={sliceColor}
                      coverRadius={0.45}
                      coverFill={"#FFF"}
                    />
                    <View style={styles.userCountsContainer}>
                      <View style={styles.firstCountContainer}>
                        <View style={styles.firstCountRectangle} />
                        <HiraginoKakuText style={styles.firstCountText}>
                          男性：
                          <HiraginoKakuText style={{ color: colors.primary }}>
                            30
                          </HiraginoKakuText>
                          人
                        </HiraginoKakuText>
                      </View>
                      <View style={styles.secondCountContainer}>
                        <View style={styles.secondCountRectangle} />
                        <HiraginoKakuText style={styles.secondCountText}>
                          女性：
                          <HiraginoKakuText style={{ color: colors.primary }}>
                            50
                          </HiraginoKakuText>
                          人
                        </HiraginoKakuText>
                      </View>
                      <View style={styles.secondCountContainer}>
                        <View style={styles.thirdCountRectangle} />
                        <HiraginoKakuText style={styles.secondCountText}>
                          未入力：
                          <HiraginoKakuText style={{ color: colors.primary }}>
                            20
                          </HiraginoKakuText>
                          人
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.paginationContainer}>
                <View style={styles.paginationHeaderContainer}>
                  <HiraginoKakuText style={styles.paginationHeaderText}>
                    受付した人
                  </HiraginoKakuText>
                </View>
                <View style={styles.lineSeparator} />
                <View style={styles.dropDownBtnContainer}>
                  <CustomButton
                    text="受付方法"
                    onPress={defaultOnPress}
                    style={styles.dropdownButton}
                    type="ButtonMGray"
                    textWidth={64}
                    icon={
                      <AntDesign
                        name="down"
                        size={14}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="behind"
                    isNormalText
                  />
                  <CustomButton
                    text="受付対象"
                    onPress={defaultOnPress}
                    style={styles.dropdownButton}
                    type="ButtonMGray"
                    textWidth={64}
                    icon={
                      <AntDesign
                        name="down"
                        size={14}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="behind"
                    isNormalText
                  />
                  <CustomButton
                    text="本人確認"
                    onPress={defaultOnPress}
                    style={styles.dropdownButton}
                    type="ButtonMGray"
                    textWidth={64}
                    icon={
                      <AntDesign
                        name="down"
                        size={14}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="behind"
                    isNormalText
                  />
                  <CustomButton
                    text="性別"
                    onPress={defaultOnPress}
                    style={styles.smallDropdownButton}
                    type="ButtonMGray"
                    textWidth={32}
                    icon={
                      <AntDesign
                        name="down"
                        size={14}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="behind"
                    isNormalText
                  />
                  <CustomButton
                    text="年代"
                    onPress={defaultOnPress}
                    style={styles.smallDropdownButton}
                    type="ButtonMGray"
                    textWidth={32}
                    icon={
                      <AntDesign
                        name="down"
                        size={14}
                        color={colors.greyTextColor}
                      />
                    }
                    iconPosition="behind"
                    isNormalText
                  />
                </View>
                <View style={styles.topPaginationContainer}>
                  <View style={styles.countContainer}>
                    <HiraginoKakuText style={styles.paginationCount} normal>
                      1-50 / 9,999 件中
                    </HiraginoKakuText>
                  </View>
                  <CustomButton
                    text="CSVダウンロード"
                    onPress={defaultOnPress}
                    style={styles.csvDownloadButton}
                    type="ButtonMediumGray"
                    textWidth={133}
                  />
                </View>
                <View style={styles.tableContainer}>
                  {/* Render the header */}
                  {renderHeader()}

                  {/* Render the table items */}
                  {getPageData().map((item, index) => (
                    <View key={index}>{renderTableItem({ item })}</View>
                  ))}
                </View>
                <View style={styles.onChangePageContainer}>
                  <View style={styles.previousButtonsContainer}>
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.skipBackButton}
                      type="ButtonSDisable"
                      icon={
                        <Feather
                          name="skip-back"
                          size={20}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="center"
                    />
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.chevronLeftButton}
                      type="ButtonSDisable"
                      icon={
                        <Feather
                          name="chevron-left"
                          size={20}
                          color={colors.greyTextColor}
                        />
                      }
                      iconPosition="center"
                    />
                  </View>
                  <View style={styles.pageNumberContainer}>
                    <CustomButton
                      text="1"
                      onPress={defaultOnPress}
                      style={styles.numOneButton}
                      type="ButtonSPrimary"
                    />
                    <CustomButton
                      text="2"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                    <CustomButton
                      text="3"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                    <HiraginoKakuText style={styles.threeDots} normal>
                      …
                    </HiraginoKakuText>
                    <CustomButton
                      text="50"
                      onPress={defaultOnPress}
                      style={styles.numSGrayButton}
                      type="ButtonSGray"
                    />
                  </View>
                  <View style={styles.nextButtonsContainer}>
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.rightButtons}
                      type="ButtonSGray"
                      icon={
                        <Feather
                          name="chevron-right"
                          size={20}
                          color={colors.textColor}
                        />
                      }
                      iconPosition="center"
                    />
                    <CustomButton
                      text=""
                      onPress={defaultOnPress}
                      style={styles.rightButtons}
                      type="ButtonSGray"
                      icon={
                        <Feather
                          name="skip-forward"
                          size={20}
                          color={colors.textColor}
                        />
                      }
                      iconPosition="center"
                    />
                  </View>
                </View>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>

      {isStatusChangeModalVisible && (
        <StatusChangeDialog
          statusName={selectedStatusName}
          onCancelButtonPress={handleCancelButton}
          onChangeButtonPress={handleCancelButton}
        />
      )}
      {isDeleteModalVisible && (
        <EventDeleteDialog
          onCancelButtonPress={handleDeleteCancelButton}
          onDeleteButtonPress={handleDeleteButton}
        />
      )}
    </SafeAreaView>
  );
};
