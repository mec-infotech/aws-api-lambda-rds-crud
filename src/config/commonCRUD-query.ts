interface LambdaResult {
    statusCode: number;
    body: string;
  }
  
  export async function invokeLambdaWithQuery(method: string, queryString: string): Promise<LambdaResult> {
    try {
      const baseURL = 'https://kcpydnsbli.execute-api.eu-west-1.amazonaws.com/dev/'; 
      const resource = "users";
      const url= baseURL + resource;
  
      const response = await fetch(url, {
        method: method,
        body: JSON.stringify({
          queryString
        })
      });
  
      const responseBody = await response.json();
  
      // console.log('ResponseBody from API Gateway:', responseBody.body); 
  
      return {
        statusCode: responseBody.statusCode,
        body: responseBody.body
      };
    } catch (error:any) {
      console.error('Error invoking Lambda function through API Gateway:', error);
      return { statusCode: 500, body: JSON.stringify({ message: 'Internal server error', error: error.toString() }) };
    }
  }

  export const executeQuery = async (method:string, queryString:string) => {
    try {
      const result = await invokeLambdaWithQuery(method, queryString);
      const parsedBody = JSON.parse(result.body);
      const status = result.statusCode;
      const data = parsedBody.result;

      let message = "";
      if (status === 400) {
        message = "No rows affected by query";
      } else if(status === 200) {
        message = "success";
      } else {
        message = "Internal Server Error.";
      }
      return { data, message };
    } catch (error) {
      console.error('Error executing query:', error);
      throw new Error('Failed to execute query');
    }
  };
  