interface LambdaResult {
    statusCode: number;
    body: string;
  }

export async function invokeLambdaThroughAPI(method: string, operation: string, table: string, data?: any, conditions?: any, orderBy?: string): Promise<LambdaResult> {
    try {
      const url = 'https://kcpydnsbli.execute-api.eu-west-1.amazonaws.com/dev/users'; 
  
      const response = await fetch(url, {
        method: method,
        body: JSON.stringify({
          operation,
          table,
          data,
          conditions,
          orderBy
        })
      });
  
      console.log('Response from API Gateway:', response); // Log the response object
  
      const responseBody = await response.json();

      console.log('ResponseBody from API Gateway:', responseBody.body); // Log the response object
  
      return {
        statusCode: response.status,
        body: responseBody.body
      };
    } catch (error:any) {
      console.error('Error invoking Lambda function through API Gateway:', error);
      return { statusCode: 500, body: JSON.stringify({ message: 'Internal server error', error: error.toString() }) };
    }
  }
  
  