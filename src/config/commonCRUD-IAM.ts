import AWS from 'aws-sdk';

interface LambdaResult {
  statusCode: number;
  body: string;
}

export async function invokeLambda(operation: string, table: string, data?: any,conditions?: any, orderBy?: string): Promise<LambdaResult> {
  try {
    // Configure AWS SDK with IAM user credentials
    AWS.config.update({
      accessKeyId: 'AKIATCKAR4JYBELEYLWQ',
      secretAccessKey: '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',
      region: 'eu-west-1' 
    });

    // Create an AWS.Lambda object
    const lambda = new AWS.Lambda();

    // Construct the Lambda invocation parameters
    const params = {
      FunctionName: 'CRUDfun',
      InvocationType: 'RequestResponse', // Synchronous invocation
      Payload: JSON.stringify({
        operation,
        table,
        data,
        conditions,
        orderBy
      })
    };

    // Invoke the Lambda function
    const response = await lambda.invoke(params).promise();

    // Extract the result from the response
    const payloadString = response.Payload?.toString(); // Convert Blob to string

    if (!payloadString) {
      console.error('Lambda function returned an undefined response body');
      return { statusCode: 500, body: JSON.stringify({ message: 'Internal server error' }) };
    }

    const { statusCode, body } = JSON.parse(payloadString);

    return { statusCode, body };
  } catch (error:any) {
    console.error('Error invoking Lambda function:', error);
    return { statusCode: 500, body: JSON.stringify({ message: 'Internal server error', error: error.toString() }) };
  }
}