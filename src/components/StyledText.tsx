import { Text, TextProps } from "./Themed";
import { fonts } from "../styles/font";
import { StyleSheet, TextStyle } from "react-native";

export function MonoText(props: TextProps) {
  return <Text {...props} style={[props.style, { fontFamily: "SpaceMono" }]} />;
}

interface CustomTextProps extends TextProps {
  normal?: boolean;
  numberOfLines?: number;
}

export function HiraginoKakuText(props: CustomTextProps) {
  const { normal, style, numberOfLines, ...rest } = props;
  const textStyle = normal ? styles.normalText : styles.boldText;

  return (
    <Text {...rest} style={[style, textStyle]} numberOfLines={numberOfLines} />
  );
}

const styles = StyleSheet.create({
  normalText: {
    fontFamily: fonts.FontRegular.fontFamily,
    fontWeight: fonts.FontRegular.weight as TextStyle["fontWeight"],
  },
  boldText: {
    fontFamily: fonts.FontBold.fontFamily,
    fontWeight: fonts.FontBold.weight as TextStyle["fontWeight"],
  },
});
