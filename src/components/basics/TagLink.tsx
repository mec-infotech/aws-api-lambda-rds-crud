import React from "react";
import {
  Pressable,
  Linking,
  StyleProp,
  TextStyle,
  ViewStyle,
} from "react-native";
import { HiraginoKakuText } from "../../components/StyledText";
import { colors } from "../../styles/color";

interface TagLinkProps {
  url: string;
  text?: string;
  pressableStyle?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  normal?: boolean;
  numberOfLines?: number;
}

export const TagLink: React.FC<TagLinkProps> = ({
  url,
  text,
  pressableStyle,
  textStyle,
  normal,
  numberOfLines,
}) => (
  <Pressable onPress={() => Linking.openURL(url)} style={pressableStyle}>
    <HiraginoKakuText
      style={[
        { color: colors.primary, textDecorationLine: "underline" },
        textStyle,
      ]}
      normal={normal}
      numberOfLines={numberOfLines}
    >
      {text}
    </HiraginoKakuText>
  </Pressable>
);
