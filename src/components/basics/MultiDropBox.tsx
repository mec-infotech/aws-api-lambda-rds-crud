import React, { useRef, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  StatusBar,
  Dimensions,
  Platform,
  Pressable,
} from "react-native";
import { Ionicons, Entypo } from "@expo/vector-icons";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

type Option = {
  id: string;
  label: string;
};

type Props = {
  initialOptions: Option[];
  initialSelectedIds?: string[];
  onCloseSortingDropdown: () => void;
};

type CheckboxProps = {
  isChecked: boolean;
  onCheck: (id: string) => void;
  label: string;
};

const Checkbox = ({ isChecked, onCheck, label }: CheckboxProps) => {
  return (
    <TouchableOpacity onPress={() => onCheck(label)} style={styles.checkbox}>
      {isChecked ? (
        <Ionicons name="ios-checkbox" size={24} color="#007AFF" />
      ) : (
        <Ionicons name="ios-square-outline" size={24} color="#B8BCC7" />
      )}
      <Text style={styles.checkboxLabel}>{label}</Text>
    </TouchableOpacity>
  );
};

export const MultiDropBox: React.FC<Props> = ({
  initialOptions,
  initialSelectedIds,
  onCloseSortingDropdown,
}) => {
  const [selectedIds, setSelectedIds] = useState<string[]>(
    initialSelectedIds || []
  );
  const [dropdownVisible, setDropdownVisible] = useState(false);
  const containerRef = useRef<View>(null);

  // DropDown Top
  const [containerHeight, setContainerHeight] = useState(0);
  const [dropdownHeight, setDropdownHeight] = useState(0);
  const [containerTop, setContainerTop] = useState(0);

  // SET Selected Options
  const isSelected = (id: string) => selectedIds.includes(id);
  const handleSelectOption = (id: string) => {
    const isSelected = selectedIds.includes(id);
    if (isSelected) {
      setSelectedIds(selectedIds.filter((selectedId) => selectedId !== id));
    } else {
      setSelectedIds([...selectedIds, id]);
    }
  };

  // CALCULATE DropDown Top Position
  const onContainerLayout = () => {
    containerRef.current!.measure((x, y, width, height, pageX, pageY) => {
      setContainerHeight(height);
      setContainerTop(pageY);
    });
  };

  const onDropdownLayout = (event: any) => {
    const { height } = event.nativeEvent.layout;
    setDropdownHeight(height);
  };

  const calculateDropdownTop = () => {
    const windowHeight = Dimensions.get("window").height;
    const spaceBelowContainer = windowHeight - containerTop - containerHeight;
    if (spaceBelowContainer >= dropdownHeight) {
      return containerHeight;
    } else {
      return -dropdownHeight;
    }
  };

  return (
    <View
      style={styles.container}
      ref={containerRef}
      onLayout={onContainerLayout}
    >
      <StatusBar barStyle="dark-content" />
      <Pressable
        onPress={() => {
          setDropdownVisible(!dropdownVisible);
          onCloseSortingDropdown();
        }}
      >
        <View style={styles.selectionArea}>
          <View style={styles.selectedChips}>
            {selectedIds.map((id) => {
              const option = initialOptions.find((opt) => opt.id === id);
              if (!option) return null;
              return (
                <View key={id} style={styles.chip}>
                  <Text style={styles.chipLabel}>{option.label}</Text>
                  <TouchableOpacity
                    onPress={() => handleSelectOption(id)}
                    style={styles.closeButton}
                  >
                    <Text style={styles.closeButtonText}>✕</Text>
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        </View>
        <View style={styles.dropdownToggle}>
          <View style={styles.dropdownIconContainer}>
            <Entypo
              name={"chevron-down"}
              size={22}
              style={styles.dropdownToggleText}
            />
          </View>
        </View>
      </Pressable>
      {dropdownVisible && (
        <View
          style={[styles.dropdownWrapper, { top: calculateDropdownTop() }]}
          onLayout={onDropdownLayout}
        >
          <ScrollView>
            {initialOptions.map((option) => (
              <Checkbox
                key={option.id}
                isChecked={isSelected(option.id)}
                onCheck={() => handleSelectOption(option.id)}
                label={option.label}
              />
            ))}
          </ScrollView>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: wp(30.5),
    justifyContent: "center",
  },
  selectionArea: {
    minHeight: 44,
    backgroundColor: "#FFFFFF",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    borderWidth: 1,
    borderColor: "#B8BCC7",
    borderRadius: 6,
  },
  selectedChips: {
    width: wp(28),
    flexDirection: "row",
    flexWrap: "wrap",
    paddingVertical: 5.2,
    paddingHorizontal: 9,
    gap: 4,
  },
  chip: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#EEEFF1",
    borderRadius: 4,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
  chipLabel: {
    color: "black",
    fontSize: 14,
    fontWeight: "300",
  },
  closeButton: {
    marginLeft: 8,
    height: 24,
    justifyContent: "center",
  },
  closeButtonText: {
    color: "#515867",
    fontSize: 18,
  },
  dropdownToggle: {
    position: "absolute",
    top: 5.5,
    right: 8,
  },
  dropdownIconContainer: {
    borderRadius: 20,
    padding: 5,
    width: 30,
    height: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  dropdownToggleText: {
    fontSize: 22,
    color: "rgba(156, 162, 177, 1)",
  },
  dropdownWrapper: {
    shadowColor: "rgba(8, 18, 26, 0.12)",
    shadowOffset: { width: 0, height: -1 },
    shadowOpacity: 1,
    shadowRadius: 6,
    ...Platform.select({
      web: {
        boxShadow: "0 3px 14px 0 rgba(8, 18, 26, 0.12)",
      },
    }),
    position: "absolute",
    width: wp(26.8),
    paddingVertical: 8,
    paddingHorizontal: 4,
    zIndex: 100,
    borderRadius: 6,
    backgroundColor: "#FFFFFF",
  },
  checkbox: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: 8,
    marginLeft: 8,
  },
  checkboxLabel: {
    marginLeft: 8,
    fontSize: 16,
  },
});
